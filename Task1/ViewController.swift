//
//  ViewController.swift
//  Task1
//
//  Created by student3 on 8.09.21.
//

import UIKit

class ViewController: UIViewController {
    
    private var titleDone = "Done"
    
    var selectedLanguage: String?
    var languageList = ["Englisch", "Беларускi", "Русский"]
    var selectedText: String?
    var translateText = ["en", "be", "ru"]
    
    var buttonDone = UIBarButtonItem()
    
    let textField = UITextField()
    let imageView = UIImageView()
    let buttonLight = UIButton()
    let buttonDark = UIButton()
    let buttonUnspecified = UIButton()
    let label = UILabel()
    
    let img = UIImage(named: "language.png")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        textField.text = languageList[0]
        textField.borderStyle = .roundedRect
        textField.textAlignment = .center
        textField.font = UIFont(name: "Baskerville-Italic", size: 23)
        
        imageView.image = img
        imageView.backgroundColor = .systemGray3
        
        label.text = "Hello World!"
        label.textAlignment = .center
        label.backgroundColor = .systemGray5
        label.font = UIFont(name: "Baskerville-Italic", size: 23)
        
        buttonLight.backgroundColor = .systemGray2
        buttonLight.setTitle("Light", for: .normal)
        buttonLight.addTarget(self, action: #selector(buttonLightAct), for: .touchUpInside)
        
        buttonDark.backgroundColor = .systemGray
        buttonDark.setTitle("Dark", for: .normal)
        buttonDark.setTitleColor(.black, for: .normal)
        buttonDark.setTitleColor(.blue, for: .highlighted)
        buttonDark.addTarget(self, action: #selector(buttonDarkAct), for: .touchUpInside)
        
        buttonUnspecified.setTitleColor(.darkText, for: .normal)
        buttonUnspecified.setTitle("Auto", for: .normal)
        buttonUnspecified.addTarget(self, action: #selector(buttonUnspecifiedAct), for: .touchUpInside)

        self.initView()
        myView()
        initConstraints(items: imageView, multiplierHorizontal: 1, multiplierVertical: 0.45, width: 75, height: 75)
        initConstraints(items: label, multiplierHorizontal: 1, multiplierVertical: 0.75, width: 200, height: 40)
        initConstraints(items: textField, multiplierHorizontal: 1, multiplierVertical: 1, width: 175, height: 40)
        initConstraints(items: buttonLight, multiplierHorizontal: 0.5, multiplierVertical: 1.3, width: 90, height: 40)
        initConstraints(items: buttonDark, multiplierHorizontal: 1, multiplierVertical: 1.3, width: 90, height: 40)
        initConstraints(items: buttonUnspecified, multiplierHorizontal: 1.5, multiplierVertical: 1.3, width: 90, height: 40)
    }
    
    override func viewDidLayoutSubviews() {
        self.buttonLight.layer.cornerRadius = self.buttonLight.frame.size.height/2
        self.buttonLight.layer.masksToBounds = true
        buttonLight.layer.borderWidth = 1.5
        buttonLight.layer.borderColor = UIColor.systemGray5.cgColor
        
        self.buttonDark.layer.cornerRadius = self.buttonDark.frame.size.height/2
        self.buttonDark.layer.masksToBounds = true
        buttonDark.layer.borderWidth = 1.5
        buttonDark.layer.borderColor = UIColor.systemGray5.cgColor
        
        self.buttonUnspecified.layer.cornerRadius = self.buttonUnspecified.frame.size.height/2
        self.buttonUnspecified.layer.masksToBounds = true
        buttonUnspecified.layer.borderWidth = 1.5
        buttonUnspecified.layer.borderColor = UIColor.systemGray5.cgColor
    }
    
    private func myView(){
        self.view.backgroundColor = .systemBackground
        self.view.addSubview(label)
        self.view.addSubview(imageView)
        self.view.addSubview(textField)
        self.view.addSubview(buttonLight)
        self.view.addSubview(buttonDark)
        self.view.addSubview(buttonUnspecified)
    }
    
    private func initConstraints(items: UIView, multiplierHorizontal: Double, multiplierVertical: Double, width: Int, height: Int){
        items.translatesAutoresizingMaskIntoConstraints = false
        let horizontalConstraint = NSLayoutConstraint(item: items, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: CGFloat(multiplierHorizontal), constant: 0)
        let verticalConstraint = NSLayoutConstraint(item: items, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: CGFloat(multiplierVertical), constant: 0)
        let widthConstraint = NSLayoutConstraint(item: items, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: CGFloat(width))
        let heightConstraint = NSLayoutConstraint(item: items, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: CGFloat(height))
        self.view.addConstraints([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
    }
    
    private func initView() {
        
        let pickerView = UIPickerView()
        pickerView.delegate = self
        textField.inputView = pickerView
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        buttonDone = UIBarButtonItem(title: titleDone, style: .done, target: self, action: #selector(self.action))
        
        toolBar.setItems([buttonDone], animated: true)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    
    @objc func buttonLightAct(sender: UIButton!){
        view.overrideUserInterfaceStyle = .light
        //buttonLight.setTitleColor(.black, for: .normal)
        buttonUnspecified.setTitleColor(.darkText, for: .normal)
        buttonLight.isEnabled = false
        buttonDark.isEnabled = true
        buttonUnspecified.isEnabled = true
    }
    
    @objc func buttonDarkAct(sender: UIButton!){
        view.overrideUserInterfaceStyle = .dark
        buttonDark.setTitleColor(.black, for: .normal)
        buttonUnspecified.setTitleColor(.lightText, for: .normal)
        buttonLight.isEnabled = true
        buttonDark.isEnabled = false
        buttonUnspecified.isEnabled = true
    }
    
    @objc func buttonUnspecifiedAct(sender: UIButton!){
        view.overrideUserInterfaceStyle = .unspecified
        buttonLight.isEnabled = true
        buttonDark.isEnabled = true
        buttonUnspecified.isEnabled = false
        buttonUnspecified.setTitleColor(.systemGray, for: .normal)
    }
    
    @objc func action() {
        view.endEditing(true)
        titleDone = "Done".localized(selectedText!)
        label.text = "Hello World!".localized(selectedText!)
        buttonLight.setTitle("Light".localized(selectedText!), for: .normal)
        buttonDark.setTitle("Dark".localized(selectedText!), for: .normal)
        buttonUnspecified.setTitle("Auto".localized(selectedText!), for: .normal)
        self.buttonDone.title = titleDone
    }
    
}

//MARK: - UIPickerViewDelegate
extension ViewController: UIPickerViewDelegate{
}

//MARK: - UIPickerViewDataSource
extension ViewController: UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return languageList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return languageList[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedLanguage = languageList[row]
        selectedText = translateText[row]
        textField.text = selectedLanguage
    }
}

//MARK: - Localized
extension String {
func localized(_ lang:String) ->String {

    let path = Bundle.main.path(forResource: lang, ofType: "lproj")
    let bundle = Bundle(path: path!)

    return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
}}

/*
//MARK: - UITextFieldDelegate
extension ViewController: UITextFieldDelegate{
    
}
*/
